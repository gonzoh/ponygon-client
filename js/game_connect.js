

var game = new Game();

function renderBoard(nodes) {
    for (var x = 0; x < 3; x++)
    {
        var buf = '';
        for (var y = 0; y< 3; y++)
        {
            var s = nodes[x][y].stone;
            if (s) {
                $("#a_"+x+"_"+y).html(s.player);
            } else {
            }
        }
    }
}

function runGame() {

    function onEvent(e) {
        console.log(e);
    }

    game.addEventListener(onEvent);

    game.loadGameState(null);
    tickGame();
}

function tickGame(playerInput) {
    var res = game.tick(playerInput);
    renderBoard(game.getNodes());

    if (res) {
        setTimeout(tickGame,0);
    }
}

function onBoardClick(x,y) {
    var node = game.getNodes()[x][y];
    tickGame(node);
}

$(document).ready(runGame);

