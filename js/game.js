
function Game()
{
    var self = this;

    var nodes = [[],[],[]];
    var rows = [];
    var players = [1,2];
    var currentPlayer = players[0];

    self.getNodes = function() {
        return nodes;
    };
    self.getStack = function() {
        return stack;
    };

    function setupEmptyBoard()
    {
        var index = 0;
        for (var x = 0; x < 3; x++)
        {
            for (var y = 0; y< 3; y++)
            {
                index = y*3+x;
                nodes[x].push({x:x,y:y,index:index});
            }
        }
    }

    self.loadGameState = function(gameState)
    {
        if (!gameState) {
            setupEmptyBoard();
        } else {
            nodes = gameState.nodes;
            currentPlayer = gameState.currentPlayer;
        }

        afterGameLoaded();

        queueTurn();
    };

    self.toGameState = function()
    {
        return {
            nodes:nodes,
            currentPlayer:currentPlayer
        }
    };

    function afterGameLoaded()
    {
        rows = [];

        for (var x = 0; x < 3; x++) {
            rows.push([nodes[x][0], nodes[x][1], nodes[x][2]]);
            rows.push([nodes[0][x], nodes[1][x], nodes[2][x]]);
        }

        rows.push([nodes[0][0], nodes[1][1], nodes[2][2]]);
        rows.push([nodes[2][0], nodes[1][1], nodes[0][2]]);
    }


    function generateStoneForPlayer(player) { return {player:player}; }
    function isNodeEmpty(node) {
        return !node.stone;
    }
    function placeStoneOnNode(stone, node) {
        node.stone = stone;
    }
    function rowWithThreeStonesOfPlayer(player) {
        for (var i = 0; i < rows.length; i++)
        {
            var row = rows[i];
            var stones = row.map(function(node){return node.stone;})
                .filter(function(stone) { return stone != null; });
            if (stones.length == 3) {
                if (stones[0].player == stones[1].player
                    && stones[1].player == stones[2].player
                    && stones[2].player == player) {
                    return true;
                }
            }
        }
        return false;
    }


    function nextPlayer() {
        var idx = players.indexOf(currentPlayer);
        if (idx >= players.length - 1) {
            idx = 0;
        } else {
            idx = idx + 1;
        }
        return players[idx];
    }


    var stack = [];

    function queueTurn()
    {
        for (var i=TURN.length - 1; i>=0;i--) {
            stack.push(TURN[i]);
        }
    }

    var listeners = [];
    self.addEventListener = function(listener) {
        listeners.push(listener);
    };
    self.emitEvent = function(event) {
        listeners.forEach(function(l) {
            l(event);
        })
    };

    self.tick = function(playerInput) {
        if (stack.length > 0) {
            var action = stack[stack.length - 1];
            self.emitEvent(action.text);
            if (action.playerInput && !playerInput) {
                self.emitEvent("input required: "+action.playerInput.join(','));
                return false;
            } else {
                var res = action.execute(playerInput);
                if (res) {
                    stack.pop();
                }
                return res;
            }
        }
    };

    var TURN = [
        {
            text:"place a stone of your color on an empty node",
            playerInput:["node"],
            execute:function(targetNode) {
                if (!isNodeEmpty(targetNode)) {
                    self.emitEvent("Node is not empty");
                    return false;
                }

                var stone = generateStoneForPlayer(currentPlayer);
                placeStoneOnNode(stone,targetNode);

                return true;
            }
        },
        {
            text:"if there is a row containing 3 of you stones, you win",
            execute:function() {
                if (rowWithThreeStonesOfPlayer(currentPlayer)) {
                    self.emitEvent("Player "+currentPlayer + " has won the game.");
                    self.emitEvent("GAME OVER");
                }
                return true;
            }
        },
        {
            text:"hand over to next player",
            execute: function() {
                currentPlayer = nextPlayer();
                self.emitEvent("TURN OVER");

                return true;
            }
        }
    ];

}


